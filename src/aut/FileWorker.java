package aut;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * this is a class that all of its methods are static
 * and all works with Files that program needed
 * do by the methods of this class
 * @author AmirHossein Rasulian
 */
public class FileWorker
{
    public FileWorker(){}

    /**
     * this method adds a new download data to its file
     * @param newDownload the download we want add to file
     */
    public static void addToDownLoadList (NewDownload newDownload){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\list.jdm", true));

            writer.append(newDownload.getDownLoadInformation());
            writer.append('\n');

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method deletes a download data from its file
     * @param urlAddress url adress of a download we want to delete
     */
    public static void removeFromDownloadList (String urlAddress){
        ArrayList<String> fileDatas = new ArrayList<>();
        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\list.jdm"));
            String data ;
            while ( (data = reader.readLine()) !=null)
                fileDatas.add(data);

            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0 ; i < fileDatas.size() ; i ++){
            if (fileDatas.get(i).equals(urlAddress))
            {
                fileDatas.remove(i);
                i -- ;
                fileDatas.remove(i + 1);
                i -- ;
                fileDatas.remove(i + 2);
                i -- ;
                fileDatas.remove(i + 3);
                i -- ;
                fileDatas.remove( i + 4);
                i --;
                fileDatas.remove(i + 5);
                i --;
                fileDatas.remove( i + 6);
                i --;
                fileDatas.remove(i + 7);
                i --;
                break;
            }
        }

        BufferedWriter writer = null ;

        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\list.jdm"));
            for (String str:
                 fileDatas) {
                writer.write(str);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method returns an array list of downloads that are in file
     * it first create the objects by the data given form file and then adds it ro the array list
     * and finally returns it
     * @return array list of downloads
     */
    public static ArrayList<NewDownload> getNewDownLoads (){
        ArrayList<NewDownload> newDownloads = new ArrayList<>();
        File f = (new File(".\\files\\list.jdm") ) ;
        if (!f.exists()) {
            try {
                new FileWriter(".\\files\\list.jdm");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\list.jdm"));
            String url ;
            String fileName ;
            String saveAddress ;
            String byteDownloaded ;
            String wholeBytes ;
            String startDate ;
            String directory ;
            while (( url = reader.readLine() )!= null && ( fileName = reader.readLine() )!= null && ( saveAddress = reader.readLine() )!= null && ( byteDownloaded = reader.readLine() )!= null &&( wholeBytes = reader.readLine() )!= null && ( startDate = reader.readLine() )!= null && ( directory = reader.readLine() )!= null ) {
                newDownloads.add(new NewDownload(url, fileName , saveAddress, Long.parseLong(byteDownloaded), Long.parseLong(wholeBytes) , startDate , directory));

                reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newDownloads;
    }

    /**
     * this method change a look and feel in its file
     * @param lookAndFeelName new look and feel that we want
     */
    public static void changeLookAndFeel (String lookAndFeelName){
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\lookAndFeel.jdm"));

                if (lookAndFeelName.equals("Metal Look And Feel") )
                    writer.write("javax.swing.plaf.metal.MetalLookAndFeel");

                if (lookAndFeelName.equals("Windows Look And Feel") )
                    writer.write("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

                if (lookAndFeelName.equals("Motif Look And Feel") )
                    writer.write("com.sun.java.swing.plaf.motif.MotifLookAndFeel");

                if (lookAndFeelName.equals("Windows Classic Look And Feel") )
                    writer.write("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");

                writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method return the look and feel that had saved in the file
     * @return look and feel that program starts with
     */
    public static String getLookAndFeel (){

        File f = (new File(".\\files\\lookAndFeel.jdm") ) ;
        if (!f.exists()) {
            try {
                new FileWriter(".\\files\\lookAndFeel.jdm");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedReader reader = null ;
        String lookAndFeel = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\lookAndFeel.jdm"));
            lookAndFeel = reader.readLine() ;
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (lookAndFeel == null)
            return "javax.swing.plaf.metal.MetalLookAndFeel" ;
        else
            return lookAndFeel;
    }

    /**
     * this method add file name and url of a removed download to a file for back up goals
     * @param newDownload download that has removed
     */
    public static void addToRemovedList(NewDownload newDownload){
        BufferedWriter writer = null ;

        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\removed.jdm" , true));
            writer.append(newDownload.getDownloader().getFileName());
            writer.append('\n');
            writer.append(newDownload.getBackUpInformation());
            writer.append('\n');

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method change the state of a download that is in progress
     * @param newDownload download that is in progress
     */
    public static void changeBytesDownloadedInfile (NewDownload newDownload){
        removeFromDownloadList(newDownload.getUrl().toString());
        addToDownLoadList(newDownload);
    }

    /**
     * this method reWrites the setting file every time that setting frame closes
     * @param sameTimeDownloads number of same time download
     * @param defaultSaveLocation path for default saving location
     */
    public static void changeSettings (Integer sameTimeDownloads , String defaultSaveLocation , String language , Long startDateLong , Long endDateLong , Long dateAfterLong , String isSchOnline){
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\setting.jdm"));
            writer.write(sameTimeDownloads.toString());
            writer.newLine();
            writer.write(defaultSaveLocation);
            writer.newLine();
            writer.write(language);
            writer.newLine();
            writer.write(startDateLong.toString());
            writer.newLine();
            writer.write(endDateLong.toString());
            writer.newLine();
            writer.write(dateAfterLong.toString());
            writer.newLine();
            writer.write(isSchOnline);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method returns the setting data in file for every time program starts
     * @return array list of setting data
     */
    public static ArrayList<String> getSettigsData (){
        ArrayList<String> data = new ArrayList<>();
        File f = new File(".\\files\\setting.jdm") ;
        if (!f.exists())
        {
            data.add("5");
            data.add("C:\\Users\\AHR96\\Desktop");
            data.add("English");
            data.add("0");
            data.add("0");
            data.add("0");
            data.add("false");
            return data;
        }
        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\setting.jdm"));
            data.add(reader.readLine());
            data.add(reader.readLine());
            data.add(reader.readLine());
            data.add(reader.readLine());
            data.add(reader.readLine());
            data.add(reader.readLine());
            data.add(reader.readLine());
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * this method return an string array of filter sites that user entered
     * @return string array of filter sites
     */
    public static String[] getFilterListNames (){

        ArrayList<String> data = new ArrayList<>();

        File f = new File(".\\files\\filter.jdm") ;
        if (!f.exists())
            return null ;

        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\filter.jdm"));
            String address ;
            while ( (address = reader.readLine()) != null && (!address.equals("")))
                data.add(address);
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] outPut = new String[data.size() + 1];
        outPut[0] = "" ;
        for (int i = 0 ; i < data.size() ; i ++) {
            outPut[i + 1] = new String( data.get(i) );
        }

        return outPut;
    }

    /**
     * this method add a new site to filter file
     * @param siteUrl site we want to filter
     */
    public static void addToFilterList (String siteUrl){
        BufferedWriter writer = null ;

        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\filter.jdm" , true));
            writer.append(siteUrl);
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method removes a filter site from file and its list
     * @param siteUrl the site we want to un filter
     */
    public static void removeFromFilterList (String siteUrl){
        if ( !(new File(".\\files\\filter.jdm")).exists() )
            return;
        ArrayList<String> data = new ArrayList<>();
        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new FileReader(".\\files\\filter.jdm"));
            String address ;
            while ( (address = reader.readLine()) != null && (!address.equals("")))
                data.add(address);
            reader.close();
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0 ; i < data.size() ; i ++){
            if (data.get(i).equals(siteUrl)) {
                data.remove(i);
                i -- ;
            }
        }
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\filter.jdm"));
            for (String string:
                    data) {
                writer.write(string);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method return an array list of the download datas in the queue file
     * @return an array list of queue
     */
    public static ArrayList<String> getQueues (){
        ArrayList<String> queues = new ArrayList<>();
        if (!(new File(".\\files\\queue.jdm")).exists())
            return queues;

        BufferedReader reader = null ;

        try {
            reader = new BufferedReader(new FileReader(".\\files\\queue.jdm"));
            String input ;
            while ((input = reader.readLine()) != null && !input.equals("")){
                queues.add(input);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return queues;
    }

    /**
     * this method add a new Download data to queue file
     * @param str data of new download we want to add to queue
     */
    public static void addToQueue (String str){
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\queue.jdm" , true));
            writer.append(str);
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method saves the changes of queue in setting panel in the file and renews the file
     * @param listModel a list of changed datas in queue
     */
    public static void saveToQueue (DefaultListModel<String> listModel){
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\queue.jdm"));
            for (int i = 0 ; i < listModel.size() ; i ++){
                writer.write(listModel.getElementAt(i));
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method creates a zip file from files folder that has data of program
     * it uses the next methods to do this
     */
    public static void export () {
        File directoryToZip = new File(".\\files");

        List<File> fileList = new ArrayList<File>();
        getAllFiles(directoryToZip, fileList);
        writeZipFile(directoryToZip, fileList);
    }
    public static void getAllFiles(File dir, List<File> fileList) {
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                fileList.add(file);
                if (file.isDirectory()) {
                    System.out.println("directory:" + file.getCanonicalPath());
                    getAllFiles(file, fileList);
                } else {
                    System.out.println("     file:" + file.getCanonicalPath());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeZipFile(File directoryToZip, List<File> fileList) {

        try {
            FileOutputStream fos = new FileOutputStream(  "Export.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : fileList) {
                if (!file.isDirectory()) { // we only zip files, not directories
                    addToZip(directoryToZip, file, zos);
                }
            }

            zos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException,
            IOException {

        FileInputStream fis = new FileInputStream(file);

        // we want the zipEntry's path to be a relative path that is relative
        // to the directory being zipped, so chop off the rest of the path
        String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
                file.getCanonicalPath().length());
        System.out.println("Writing '" + zipFilePath + "' to zip file");
        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

    public static void removeFromQueue (String queueData){
        ArrayList<String> data = getQueues() ;
        for (int i = 0 ; i < data.size() ; i ++){
            if ( data.get(i).equals(queueData)){
                data.remove(i);
                i --;
            }
        }
        BufferedWriter writer = null ;
        try {
            writer = new BufferedWriter(new FileWriter(".\\files\\queue.jdm"));
            for (String str:
                 data) {
                writer.write(str);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
