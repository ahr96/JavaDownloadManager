package aut;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HandleSchedular implements Runnable {
    public boolean scheduleIsOnline ;
    public Date startTime ;
    public Date endTime ;
    public int numberInQueue ;
    public static ArrayList<String> queueData ;

    public HandleSchedular (boolean scheduleIsOnline , Date startTime , Date endTime , int numberInQueue){
        this.scheduleIsOnline = scheduleIsOnline ;
        this.startTime = startTime ;
        this.endTime = endTime ;
        this.numberInQueue = numberInQueue ;
        System.out.println(numberInQueue);
        this.queueData = FileWorker.getQueues() ;
    }
    @Override
    public void run() {
        Date now;
        boolean downloadStarted ;
        Calendar nowCal = Calendar.getInstance();
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endTime);
        startCal.setTime(startTime);

        while (true){
            int k = 0 ;
            now = new Date() ;
            nowCal.setTime(now);
            if (scheduleIsOnline){
                if (nowCal.get(Calendar.HOUR) > startCal.get(Calendar.HOUR) || ( ( nowCal.get(Calendar.HOUR) == startCal.get(Calendar.HOUR) ) && ( nowCal.get(Calendar.MINUTE) > startCal.get(Calendar.MINUTE) ) )){

                    System.out.println("TIME");
                    for ( int i = 0 ; i < numberInQueue && k < queueData.size(); i ++){
                        downloadStarted = MiddlePanel.startDownloadInQueue(queueData.get(k));
                        if (!downloadStarted)
                            i -- ;
                        else
                            queueData.remove(k);
                        k ++ ;
                    }
                    break;
                }
            }
            else {
                for ( int i = 0 ; i < numberInQueue && k < queueData.size(); i ++){
                    downloadStarted = MiddlePanel.startDownloadInQueue(queueData.get(k));
                    if (!downloadStarted) {
                        i --;
                    }
                    else {
                        queueData.remove(k);
                    }
                    k ++ ;
                }
                break;
            }
        }

        // handle the ends time
        while (scheduleIsOnline) {
            if (nowCal.get(Calendar.HOUR) > endCal.get(Calendar.HOUR) || ((nowCal.get(Calendar.HOUR) == endCal.get(Calendar.HOUR)) && (nowCal.get(Calendar.MINUTE) >= endCal.get(Calendar.MINUTE)))) {
                MiddlePanel.endDownloadInQueue();
                JOptionPane.showMessageDialog(new JFrame() , "DownLoad Schedule Has Finished");
                break;
            }
        }
    }
}
