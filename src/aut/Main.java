package aut;

import java.io.IOException;

/**
 * main class of program
 * @author Amirhossein Rasulian
 */
public class Main {
    public static SettingFrame settingFrame;
    public static void main(String[] args) throws IOException {
        ThreadPool threadPool = new ThreadPool();
        settingFrame = new SettingFrame();
        HandleSchedular handleSchedular = new HandleSchedular(SettingFrame.scheduleOnline , SettingFrame.dateModel.getDate() , SettingFrame.dateModel2.getDate() , (Integer)SettingFrame.sameTimeDownloadsSpinner.getValue());

        ProgramFrame mainFrame = new ProgramFrame();
        mainFrame.showFrame();

        ThreadPool.execute(handleSchedular);
    }
}
