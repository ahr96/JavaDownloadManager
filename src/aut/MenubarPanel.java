package aut;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * a class that represents the menu bar in the program frame that has two menu
 * and they have their menu items
 * @author AmirHossein Rasulian
 */
public class MenubarPanel
{
    private JMenuBar menuBar ;
    private JMenuItem newDownLoad ;
    private JMenuItem pause ;
    private JMenuItem resume ;
    private JMenuItem cancel ;
    private JMenuItem remove ;
    private JMenuItem setting ;
    private JMenuItem exit ;
    private JMenuItem export ;

    private JMenuItem about ;
    private JFrame aboutFrame ;

    public MenubarPanel ()
    {
        HandleActions handleActions = new HandleActions();

        menuBar = new JMenuBar();

        JMenu download = new JMenu("Download");
        download.setMnemonic(KeyEvent.VK_D);
        if (SettingFrame.language.equals("Persian"))
            download.setText("دانلود");

        newDownLoad = new JMenuItem("New Download  ",KeyEvent.VK_N);
        KeyStroke controlN = KeyStroke.getKeyStroke("control N");
        newDownLoad.setAccelerator(controlN);
        newDownLoad.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            newDownLoad.setText("دانلود جدید");

        pause = new JMenuItem("Pause");
        KeyStroke controlP = KeyStroke.getKeyStroke("control P");
        pause.setAccelerator(controlP);
        pause.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            pause.setText("توقف");

        resume = new JMenuItem("Resume");
        KeyStroke controlR = KeyStroke.getKeyStroke("control R");
        resume.setAccelerator(controlR);
        resume.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            resume.setText("ادامه");

        cancel = new JMenuItem("Cancel");
        KeyStroke controlC = KeyStroke.getKeyStroke("control C");
        cancel.setAccelerator(controlC);
        cancel.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            cancel.setText("قطع کردن");

        remove = new JMenuItem("Remove");
        KeyStroke controlD = KeyStroke.getKeyStroke("control D");
        remove.setAccelerator(controlD);
        remove.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            remove.setText("پاک کردن");

        setting = new JMenuItem("Setting");
        KeyStroke controlS = KeyStroke.getKeyStroke("control S");
        setting.setAccelerator(controlS);
        setting.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            setting.setText("تنظیمات");

        exit = new JMenuItem("Exit");
        KeyStroke controlE = KeyStroke.getKeyStroke("control E");
        exit.setAccelerator(controlE);
        exit.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            exit.setText("خروج");

        export = new JMenuItem("Export");
        KeyStroke controlX = KeyStroke.getKeyStroke("control X");
        export.setAccelerator(controlX);
        export.addActionListener(handleActions);
        if (SettingFrame.language.equals("Persian"))
            export.setText("فشرده سازی");

        download.add(newDownLoad);
        download.add(resume);
        download.add(pause);
        download.add(cancel);
        download.add(remove);
        download.add(setting);
        download.add(export);
        download.add(exit);

        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);
        if (SettingFrame.language.equals("Persian"))
            help.setText("کمک");

        about = new JMenuItem("About");
        if (SettingFrame.language.equals("Persian"))
            about.setText("درباره");
        KeyStroke controlA = KeyStroke.getKeyStroke("control A");
        about.setAccelerator(controlA);
        about.addActionListener(handleActions);
        aboutFrame = new JFrame();
        aboutFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        aboutFrame.setSize(300, 200);
        aboutFrame.setResizable(false);
        aboutFrame.setBackground(Color.CYAN);
        aboutFrame.setLocation(370,240);
        JPanel aboutMainPanel = new JPanel(new GridLayout(5,1));
        aboutFrame.setContentPane(aboutMainPanel);

        if (SettingFrame.language.equals("English")) {
            aboutMainPanel.add(new JLabel("Programmed by : AmirHossein Rasulian "));
            aboutMainPanel.add(new JLabel("Student number : 9631033"));
            aboutMainPanel.add(new JLabel("Date of Start : 7/16 "));
            aboutMainPanel.add(new JLabel("Date of finish :  "));
            aboutMainPanel.add(new JLabel("THIS IS A PROGRAM TO DOWNLOAD"));
        }else {
            aboutMainPanel.add(new JLabel("برنامه نویسی شده توسط : امیرحسین رسولیان"));
            aboutMainPanel.add(new JLabel("شماره دانشجویی : 9631033"));
            aboutMainPanel.add(new JLabel("زمان شروع : 25 تیر 1397"));
            aboutMainPanel.add(new JLabel("زمان خاتمه : 30 تیر 1397"));
            aboutMainPanel.add(new JLabel("این برنامه ای برای دانلود است"));
        }


        help.add(about);

        menuBar.add(download);
        menuBar.add(help);

    }

    public JMenuBar getMenuPanel() {
        return menuBar ;
    }
    private class HandleActions implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(newDownLoad))
            {
                NewDownloadFrame newDownload = new NewDownloadFrame();
                newDownload.showFrame();
            }
            if(e.getSource().equals(pause))
            {
                MiddlePanel.pauseDownload();
            }
            if(e.getSource().equals(resume))
            {
                MiddlePanel.resumeDownload();
            }
            if(e.getSource().equals(remove))
            {
                MiddlePanel.removeDownloads();
            }
            if(e.getSource().equals(cancel))
            {
                MiddlePanel.cancelDownload();
            }
            if(e.getSource().equals(setting))
            {
                ProgramFrame.settingFrame.showFrame();
            }
            if(e.getSource().equals(exit))
            {
                System.exit(0);
            }
            if(e.getSource().equals(about)){
                aboutFrame.setVisible(true);
            }
            if (e.getSource().equals(export)){
                FileWorker.export();
            }
        }
    }
}
