package aut;

import javax.naming.NamingEnumeration;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

/**
 * the main panel of the program that holds and shows the downloads
 *  this class has methods to change some part of downloads
 *  @author AmirHossein Rasulian
 */
public class MiddlePanel
{
    private static JScrollPane middlePanel ;
    private static JPanel list;
    private static SpringLayout layout;
    private static int numberOfDownLoads ;
    public static ArrayList<NewDownload> downloadsSelected ;

    public MiddlePanel()
    {
        numberOfDownLoads = 0 ;
        list = new JPanel(){
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(500 , 60 * (numberOfDownLoads + 1 ));
            }
        } ;
        middlePanel = new JScrollPane(list , JScrollPane.VERTICAL_SCROLLBAR_ALWAYS , JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        middlePanel.setBackground(Color.GRAY);
        layout = new SpringLayout();
        list.setLayout(layout);

        for (NewDownload newDownload:
             FileWorker.getNewDownLoads()) {
            list.add(newDownload );
            layout.putConstraint(SpringLayout.NORTH , newDownload , 60 * numberOfDownLoads , SpringLayout.NORTH , list);
            numberOfDownLoads ++ ;
            for (String str:
                 FileWorker.getQueues()) {
                if (newDownload.getDownLoadQueueInformation().equals(str)){
                    newDownload.setInQueue(true);
                }
            }
            ProgramFrame.mainFrame.revalidate();
            ProgramFrame.mainFrame.repaint();
        }

        ArrayList<String> sortArray = new ArrayList<>();
        sortArray.add("time");
        sortOneFactor(sortArray , "sort");
        downloadsSelected = new ArrayList<>();
    }

    public JScrollPane getMiddlePanel() {
        return middlePanel;
    }

    public static void addNewDownload (NewDownload newDownload)
    {
        FileWorker.addToDownLoadList(newDownload);

        list.add(newDownload );
        layout.putConstraint(SpringLayout.NORTH , newDownload , 60 * numberOfDownLoads , SpringLayout.NORTH , list);
        numberOfDownLoads ++ ;

        ProgramFrame.mainFrame.revalidate();
        ProgramFrame.mainFrame.repaint();

        SettingFrame.addToQueue(newDownload.getDownLoadQueueInformation());
        newDownload.setState("DOWNLOADING");
        ThreadPool.execute(newDownload.getDownloader());
    }

    public static void removeDownloads (){
        for (NewDownload newDownload:
             downloadsSelected) {
            FileWorker.addToRemovedList(newDownload);
            list.remove(newDownload);
            FileWorker.removeFromDownloadList(newDownload.getUrl().toString());
        }

        ArrayList<NewDownload> newDownloads = new ArrayList<>();
        for (Component component:
             list.getComponents()) {
            list.remove(component);
            newDownloads.add( (NewDownload)component );
        }
        numberOfDownLoads = 0 ;
        for (NewDownload newDownload:
                newDownloads) {
            list.add(newDownload);
            layout.putConstraint(SpringLayout.NORTH, newDownload, 60 * numberOfDownLoads, SpringLayout.NORTH, list);
            numberOfDownLoads ++;
        }
        ProgramFrame.mainFrame.revalidate();
        ProgramFrame.mainFrame.repaint();
    }
    public static void pauseDownload (){
        for (NewDownload newDownload:
             downloadsSelected) {
            FileWorker.changeBytesDownloadedInfile(newDownload);
            newDownload.getDownloader().pause();
            newDownload.setState("PROCESSING");
        }
    }
    public static void resumeDownload (){
        for (NewDownload newDownload:
             downloadsSelected) {
            newDownload.getDownloader().resume();
            newDownload.setState("DOWNLOADING");
            ThreadPool.execute(newDownload.getDownloader());
        }
    }
    public static void cancelDownload (){
        for (NewDownload newDownload:
             downloadsSelected) {
            newDownload.getDownloader().cancel();
            Desktop.getDesktop().moveToTrash(new File(newDownload.getDownloader().getSaveAddressExact() ));
            System.out.println(newDownload.getDownloader().getSaveAddressExact());
            newDownload.setState("CANCEL");
        }
    }
    public static void search (String searchStr){
        for (Component component:
             list.getComponents()) {
            NewDownload newDownload = (NewDownload) component;
            if (newDownload.getDownloader().getFileName().contains(searchStr) || newDownload.getUrl().toString().contains(searchStr)) {
                newDownload.setBackground(Color.yellow);
                System.out.println(newDownload.getDownloader().getFileName());
            }
        }
    }
    public static void backToNormalAfterSearch (){
        for (Component component:
                list.getComponents()) {
            NewDownload newDownload = (NewDownload) component;
            newDownload.setBackground(Color.WHITE);
        }
    }

    /**
     * a method to sort the list base on factors
     * it first give all components and remove them form list
     * then sort them
     * and then add them to list
     * @param factors factor we want sort base on
     */
    public static void sortOneFactor (ArrayList<String> factors , String kind){
        String factor = "";
        String factor2 = "";
        String factor3  = "";

        if (factors.size() == 1)
            factor = factors.get(0);
        if (factors.size() == 2){
            factor = factors.get(0);
            factor2 = factors.get(1);
        }
        if (factors.size() == 3){
            factor = factors.get(0);
            factor2 = factors.get(1);
            factor3 = factors.get(2) ;
        }

        int count = list.getComponentCount();
        NewDownload[] newDownloads = new NewDownload[count];
        for (int i = 0 ; i < count ; i ++)
            newDownloads[i] = (NewDownload) list.getComponent(i);

        for ( int i = 0 ; i < list.getComponentCount() ;){
            list.remove(list.getComponent(0));
        }
        numberOfDownLoads = 0 ;

        for (int i = 0 ; i < count - 1 ; i ++)
        {
            for (int j = 0 ; j < count - 1 ; j ++) {
                if (factor.equals("fileName")){
                    if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) > 0){
                        NewDownload swap = newDownloads[j + 1];
                        newDownloads[j + 1] = newDownloads [j];
                        newDownloads[j] = swap ;
                    }
                    if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) == 0) {
                        if (factor2.equals("time")){
                            if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) > 0){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) == 0) {
                                if (factor3.equals("size")){
                                    if (newDownloads[j].getDownloader().getWholeBytes() < newDownloads[j + 1].getDownloader().getWholeBytes()){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }
                        }
                        if (factor2.equals("size")){
                            if (newDownloads[j].getDownloader().getWholeBytes() < newDownloads[j + 1].getDownloader().getWholeBytes()){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getDownloader().getWholeBytes() == newDownloads[j + 1].getDownloader().getWholeBytes()){
                                if (factor3.equals("time")){
                                    if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) > 0){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }
                        }
                    }
                }

                if (factor.equals("time")){
                    if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) > 0){
                        NewDownload swap = newDownloads[j + 1];
                        newDownloads[j + 1] = newDownloads [j];
                        newDownloads[j] = swap ;
                    }
                    if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) == 0){
                        if (factor2.equals("fileName")){
                            if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) > 0){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) == 0){
                                if (factor3.equals("size")){
                                    if (newDownloads[j].getDownloader().getWholeBytes() < newDownloads[j + 1].getDownloader().getWholeBytes()){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }

                        }
                        if (factor2.equals("size")){
                            if (newDownloads[j].getDownloader().getWholeBytes() < newDownloads[j + 1].getDownloader().getWholeBytes()){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getDownloader().getWholeBytes() == newDownloads[j + 1].getDownloader().getWholeBytes()){
                                if (factor3.equals("fileName")){
                                    if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) > 0){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }
                        }
                    }
                }
                if (factor.equals("size")){
                    if (newDownloads[j].getDownloader().getWholeBytes() < newDownloads[j + 1].getDownloader().getWholeBytes()){
                        NewDownload swap = newDownloads[j + 1];
                        newDownloads[j + 1] = newDownloads [j];
                        newDownloads[j] = swap ;
                    }
                    if (newDownloads[j].getDownloader().getWholeBytes() == newDownloads[j + 1].getDownloader().getWholeBytes()){
                        if (factor2.equals("fileName")){
                            if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) > 0){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) == 0){
                                if (factor3.equals("time")){
                                    if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) > 0){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }
                        }
                        if (factor2.equals("time")){
                            if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) > 0){
                                NewDownload swap = newDownloads[j + 1];
                                newDownloads[j + 1] = newDownloads [j];
                                newDownloads[j] = swap ;
                            }
                            if (newDownloads[j].getStartDate().compareTo(newDownloads[j + 1].getStartDate()) == 0){
                                if (factor3.equals("fileName")){
                                    if (newDownloads[j].getDownloader().getFileName().compareTo(newDownloads[j + 1].getDownloader().getFileName()) > 0){
                                        NewDownload swap = newDownloads[j + 1];
                                        newDownloads[j + 1] = newDownloads [j];
                                        newDownloads[j] = swap ;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (kind.equals("sort")) {
            for (NewDownload newDownload :
                    newDownloads) {
                list.add(newDownload);
                layout.putConstraint(SpringLayout.NORTH, newDownload, 60 * numberOfDownLoads, SpringLayout.NORTH, list);
                numberOfDownLoads++;
            }
        }
        else {
            for ( int i = count - 1 ; i >= 0 ; i --){
                NewDownload newDownload = newDownloads[i] ;
                list.add(newDownload);
                layout.putConstraint(SpringLayout.NORTH, newDownload , 60 * numberOfDownLoads, SpringLayout.NORTH, list);
                numberOfDownLoads++;
            }
        }
        ProgramFrame.mainFrame.revalidate();
        ProgramFrame.mainFrame.repaint();

    }

    public static boolean startDownloadInQueue (String queueData){
        for (Component component:
             list.getComponents()) {
            NewDownload newDownload = (NewDownload) component;
            if (queueData.equals(newDownload.getDownLoadQueueInformation())){
                if (newDownload.getState().equals("PROCESSING")) {
                    ThreadPool.execute(newDownload.getDownloader());
                    newDownload.setState("DOWNLOADING");
                    return true;
                }
                else
                    return false;
            }
        }
        return true;
    }

    public static void endDownloadInQueue (){
        for (Component component:
             list.getComponents()) {
            NewDownload newDownload = (NewDownload) component;
            for (String str:
                 FileWorker.getQueues()) {
                if (str.equals(newDownload.getDownLoadQueueInformation())){
                    newDownload.getDownloader().pause();
                    newDownload.setState("PROCESSING");
                }
            }
        }
    }

    public static boolean checkThatDownloadExist (String urlName){
        for (Component component:
             list.getComponents()) {
            NewDownload newDownload = (NewDownload) component;
            if (newDownload.getUrl().toString().equals(urlName))
                return false;
        }
        return true;
    }
}
