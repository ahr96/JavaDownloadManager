package aut;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * this class is a thread that handles the downloading form network
 * @author AmirHossein Rasulian
 */
public class NetWorkDownloader implements Runnable{
    private URL url ;
    private String saveDirection;
    private static final int BUFFER_SIZE = 4096;

    private String fileName ;
    private String saveAddressExact ;
    private long byteDownloaded;
    private long wholeBytes ;
    private long rate ;
    private NewDownload newDownload;

    private boolean pauseState ;

    public NetWorkDownloader (NewDownload newDownload , String saveDirection) {
        pauseState = false ;
        this.newDownload = newDownload ;

        this.url = newDownload.getUrl();
        this.saveDirection = saveDirection;
        byteDownloaded = 0 ;

        int responseCode ;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            responseCode = connection.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                fileName = "";
                String disposition = connection.getHeaderField("Content-Disposition");
                String contentType = connection.getContentType();
                wholeBytes = connection.getContentLength();

                if (disposition != null) {
                    // extracts file name from header field
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        fileName = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    // extracts file name from URL
                    fileName = url.toString().substring(url.toString().lastIndexOf("/") + 1,
                            url.toString().length());
                }
                if (fileName.equals(""))
                    fileName = url.toString().substring(url.toString().lastIndexOf("/") + 1,
                            url.toString().lastIndexOf('.') + 4);


                    saveAddressExact = saveDirection + File.separator + fileName ;

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
    }
    public NetWorkDownloader (NewDownload newDownload , String fileName ,String saveAddressExact , long byteDownloaded , long wholeBytes){
        pauseState = false ;
        this.newDownload = newDownload ;
        this.saveAddressExact = saveAddressExact ;
        this.byteDownloaded = byteDownloaded ;
        this.fileName = fileName ;
        this.wholeBytes = wholeBytes ;
        this.url = newDownload.getUrl() ;

    }
    @Override
    public void run() {
        if (!pauseState && byteDownloaded < wholeBytes) {
            int responseCode;
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setReadTimeout(5000);
                connection.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
                connection.addRequestProperty("User-Agent", "Mozilla");
                connection.addRequestProperty("Referer", "google.com");

                connection.setRequestProperty("Range" , "bytes=" + byteDownloaded + "-" + wholeBytes);
                connection.connect();

                boolean redirect = false;
                responseCode = connection.getResponseCode();
                // normally, 3xx is redirect
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                            || responseCode == HttpURLConnection.HTTP_MOVED_PERM
                            || responseCode == HttpURLConnection.HTTP_SEE_OTHER)
                        redirect = true;
                }

                if (redirect) {

                    // get redirect url from "location" header field
                    String newUrl = connection.getHeaderField("Location");

                    // get the cookie if need, for login
                    String cookies = connection.getHeaderField("Set-Cookie");

                    // open the new connnection again
                    connection = (HttpURLConnection) new URL(newUrl).openConnection();
                    connection.setRequestProperty("Cookie", cookies);
                    connection.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
                    connection.addRequestProperty("User-Agent", "Mozilla");
                    connection.addRequestProperty("Referer", "google.com");

                    connection.setRequestProperty("Range" , "bytes=" + byteDownloaded + "-" + wholeBytes);
                    connection.connect();
                }

                // always check HTTP response code first
                if (responseCode == 200 || responseCode == 206) {
                    // opens input stream from the HTTP connection\\
                    InputStream inputStream = connection.getInputStream();
                    String saveFilePath = saveAddressExact.replace("\\", "\\\\");
                    // opens an output stream to save into file
                    FileOutputStream outputStream = new FileOutputStream(saveFilePath , true);

                    int bytesRead = -1;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    long startTime = System.currentTimeMillis();
                    long rateCalculator = 0;

                    while ((bytesRead = inputStream.read(buffer)) != -1 && !pauseState) {

                        if (System.currentTimeMillis() - startTime >= 1000) {
                            rate = rateCalculator;
                            rateCalculator = 0;
                            startTime += 1000;
                        }
                        byteDownloaded += bytesRead;
                        rateCalculator += bytesRead;
                        newDownload.updateData();
                        outputStream.write(buffer, 0, bytesRead);
                    }

                    outputStream.close();
                    inputStream.close();

                } else {
                    System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                }
                connection.disconnect();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getFileName() {
        return fileName;
    }

    public long getWholeBytes() {
        return wholeBytes;
    }

    public long getByteDownloaded() {
        return byteDownloaded;
    }

    public String getSaveAddressExact() {
        return saveAddressExact;
    }

    public long getRate() {
        return rate;
    }
    public void pause (){
        pauseState = true ;
        rate = 0 ;
        newDownload.updateData();
    }
    public void resume (){
        pauseState = false ;
        newDownload.setState("DOWNLOADING");
    }
    public void cancel (){
        if (byteDownloaded != 0) {
            byteDownloaded = 0;
            FileWorker.removeFromDownloadList(newDownload.getUrl().toString());
            FileWorker.addToDownLoadList(newDownload);
            pauseState = true;
            newDownload.updateData();
        }
    }
}
