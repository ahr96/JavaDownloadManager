package aut;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * this class shows a download procedure
 * @author AmirHossein Rasulian
 */
public class NewDownload extends JPanel
{
    private URL url;
    private String saveAddress ;

    private JFrame downloadInfoFrame ;
    private JPanel downloadInfoPanel ;
    private JRadioButton radioButton;
    private JButton openFileButton ;
    private NetWorkDownloader downloader;

    private JLabel percentDl ;
    private JLabel dlString ;
    private JLabel rtString ;
    private JProgressBar progressBar ;

    private String startDate ;
    private File directory ;

    private String state ;
    private boolean inQueue ;

    public NewDownload (String url , String saveAddress) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        inQueue = false ;
        this.directory = new File(saveAddress);
        downloader = new NetWorkDownloader(this , saveAddress);

        this.saveAddress = downloader.getSaveAddressExact() ;

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        startDate = dateFormat.format(date) ;
        state = "PROCESSING" ;

        makePanel();
    }
    public NewDownload (String url ,String fileName , String saveAddress , long byteDownloaded , long wholeBytes , String startDate , String directory){
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        inQueue = false ;
        this.directory = new File(directory);
        downloader = new NetWorkDownloader(this , fileName ,saveAddress , byteDownloaded , wholeBytes);

        this.saveAddress = saveAddress;
        this.startDate = startDate ;

        if (byteDownloaded < wholeBytes)
            state = "PROCESSING";
        else
            state = "COMPLETE";

        makePanel();
    }
    public void makePanel() {
        MouseHandler mouseHandler = new MouseHandler();
        HandleActions handleActions = new HandleActions();

        this.addMouseListener(mouseHandler);

        Integer percentDownloaded = (int)( downloader.getByteDownloaded() *100 / downloader.getWholeBytes() );

        Border border = BorderFactory.createLineBorder(Color.BLACK , 3);
        this.setBorder(border);
        this.setPreferredSize(new Dimension(750,60));
        this.setBackground(Color.WHITE);
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);

        JLabel dlIcon = new JLabel( new ImageIcon(".\\Icons\\dlIcon.png") );
        JLabel nameOfFile = new JLabel( downloader.getFileName() );
        percentDl = new JLabel(percentDownloaded.toString() + "%");
        dlString = new JLabel(downloadedBytesString(downloader.getByteDownloaded() , downloader.getWholeBytes()));
        rtString = new JLabel(rateString( downloader.getRate() ));

        progressBar = new JProgressBar();
        progressBar.setValue( percentDownloaded );
        progressBar.setPreferredSize(new Dimension(420 , 10));

        radioButton = new JRadioButton();
        radioButton.setBackground(Color.WHITE);
        radioButton.addActionListener(handleActions);

        openFileButton = new JButton(new ImageIcon(".\\Icons\\fileChooser.png"));
        openFileButton.setBackground(Color.WHITE);
        openFileButton.addActionListener(handleActions);

        this.add(nameOfFile);
        this.add(progressBar);
        this.add(dlIcon);
        this.add(percentDl);
        this.add(dlString);
        this.add(rtString);
        this.add(radioButton);
        this.add(openFileButton);

        layout.putConstraint(SpringLayout.WEST , dlIcon , 10 ,SpringLayout.WEST , this);
        layout.putConstraint(SpringLayout.NORTH , dlIcon, 10 , SpringLayout.NORTH , this);
        layout.putConstraint(SpringLayout.WEST , nameOfFile , 40 ,SpringLayout.WEST , this);
        layout.putConstraint(SpringLayout.NORTH , nameOfFile ,3 , SpringLayout.NORTH , this);
        layout.putConstraint(SpringLayout.WEST , progressBar, 40 , SpringLayout.WEST , this);
        layout.putConstraint(SpringLayout.NORTH , progressBar , 20, SpringLayout.NORTH , this);
        layout.putConstraint(SpringLayout.NORTH , percentDl , 17, SpringLayout.NORTH , this);
        layout.putConstraint(SpringLayout.WEST , percentDl , 10 , SpringLayout.EAST , progressBar);
        layout.putConstraint(SpringLayout.NORTH , dlString , 5, SpringLayout.SOUTH , progressBar);
        layout.putConstraint(SpringLayout.EAST , dlString , 10 , SpringLayout.EAST ,progressBar);
        layout.putConstraint(SpringLayout.NORTH , rtString , 5, SpringLayout.SOUTH , progressBar);
        layout.putConstraint(SpringLayout.WEST , rtString , 10 , SpringLayout.WEST ,progressBar);
        layout.putConstraint(SpringLayout.EAST , radioButton , 0 , SpringLayout.EAST , this);
        layout.putConstraint(SpringLayout.NORTH , radioButton, 20 , SpringLayout.NORTH , this);
        layout.putConstraint(SpringLayout.EAST , openFileButton , 0, SpringLayout.WEST , radioButton);
        layout.putConstraint(SpringLayout.NORTH , openFileButton, 15 , SpringLayout.NORTH , this);

        // create download info frame
        {

            downloadInfoFrame = new JFrame(){
                @Override
                public void paint(Graphics g) {
                    super.paint(g);
                }
            };
            downloadInfoFrame.setResizable(false);
            downloadInfoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            downloadInfoFrame.setSize(800, 500);
            downloadInfoFrame.setLocation(500, 300);
            downloadInfoPanel = new JPanel();
            downloadInfoFrame.setContentPane(downloadInfoPanel);
            SpringLayout layout1 = new SpringLayout();
            downloadInfoPanel.setLayout(layout1);

            JLabel sizeLabel = new JLabel("Size :    " + bytesPlusSuffix( downloader.getWholeBytes() )) ;
            if (SettingFrame.language.equals("Persian"))
                sizeLabel.setText(" حجم ");

            JLabel saveToLabel = new JLabel("Save to :");
            if (SettingFrame.language.equals("Persian"))
                saveToLabel.setText(" ذخیره در ");

            JTextField saveToTextField = new JTextField(saveAddress);
            saveToTextField.setEditable(false);
            saveToTextField.setPreferredSize(new Dimension(700 , 30));
            JLabel addressLabel = new JLabel("Address :    " + url.toString());
            if (SettingFrame.language.equals("Persian"))
                addressLabel.setText(":        آدرس");
            JLabel dateLabel = new JLabel("Start Download at :                   " + startDate);
            if (SettingFrame.language.equals("Persian"))
                dateLabel.setText("                         : آغاز دانلود در");

            downloadInfoPanel.add(sizeLabel);
            downloadInfoPanel.add(saveToLabel);
            downloadInfoPanel.add(saveToTextField);
            downloadInfoPanel.add(addressLabel);
            downloadInfoPanel.add(dateLabel);

            layout1.putConstraint(SpringLayout.NORTH , sizeLabel , 20 , SpringLayout.NORTH , downloadInfoPanel);
            layout1.putConstraint(SpringLayout.WEST , sizeLabel , 20 , SpringLayout.WEST , downloadInfoPanel);
            layout1.putConstraint(SpringLayout.NORTH , saveToLabel , 53 , SpringLayout.NORTH , sizeLabel);
            layout1.putConstraint(SpringLayout.WEST , saveToLabel , 20 , SpringLayout.WEST , downloadInfoPanel);
            layout1.putConstraint(SpringLayout.NORTH , saveToTextField , 50 , SpringLayout.NORTH , sizeLabel);
            layout1.putConstraint(SpringLayout.WEST , saveToTextField , 5 , SpringLayout.EAST , saveToLabel);
            layout1.putConstraint(SpringLayout.NORTH , addressLabel , 50 , SpringLayout.NORTH , saveToLabel);
            layout1.putConstraint(SpringLayout.WEST , addressLabel , 20 , SpringLayout.WEST , downloadInfoPanel);
            layout1.putConstraint(SpringLayout.NORTH , dateLabel , 50 , SpringLayout.NORTH , addressLabel);
            layout1.putConstraint(SpringLayout.WEST , dateLabel , 20 , SpringLayout.WEST , downloadInfoPanel);
        }
    }

    private String downloadedBytesString (long byteDownloaded , long wholeBytes)
    {
        return bytesPlusSuffix(byteDownloaded) + " / " + bytesPlusSuffix(wholeBytes);
    }
    private String rateString (long rate)
    {
        if (SettingFrame.language.equals("English"))
            return bytesPlusSuffix(rate) + " /sec" ;
        else
            return bytesPlusSuffix(rate) + "ثانیه/" ;
    }

    public String getDownLoadInformation (){
        return url.toString() + "\n" + downloader.getFileName() + "\n" + downloader.getSaveAddressExact() + "\n" + downloader.getByteDownloaded() + "\n" + downloader.getWholeBytes() + "\n" + startDate + "\n" + directory.getAbsolutePath() + "\n";
    }
    public String getDownLoadQueueInformation (){
        return downloader.getFileName() + "                        " + bytesPlusSuffix(downloader.getWholeBytes()) ;
    }
    public String getBackUpInformation (){
        return url.toString() ;
    }

    public NetWorkDownloader getDownloader() {
        return downloader;
    }

    public String getState() {
        return state;
    }

    public boolean isInQueue() {
        return inQueue;
    }

    public void setInQueue(boolean inQueue) {
        this.inQueue = inQueue;
    }

    class MouseHandler extends MouseAdapter{
        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e))
            {
                downloadInfoFrame.setVisible(true);
            }
            if (e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
                try {
                    if (downloader.getByteDownloaded() == downloader.getWholeBytes())
                        Desktop.getDesktop().open(new File(saveAddress));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    class HandleActions implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(radioButton))
                handleSelection();
            if (e.getSource().equals(openFileButton)) {
                try {
                    Desktop.getDesktop().open(directory);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void handleSelection (){
        if (radioButton.isSelected())
            MiddlePanel.downloadsSelected.add(this);
        else
            MiddlePanel.downloadsSelected.remove(this);
    }

    public URL getUrl() {
        return url;
    }

    public void updateData (){
        if (!state.equals("DOWNLOADING"))
            state = "PROCESSING" ;

        Integer percentDownloaded = (int)( downloader.getByteDownloaded() *100 / downloader.getWholeBytes() );
        percentDl.setText(percentDownloaded.toString() + "%");
        dlString.setText(downloadedBytesString(downloader.getByteDownloaded() , downloader.getWholeBytes()));
        rtString.setText(rateString( downloader.getRate() ));
        progressBar.setValue( percentDownloaded );

        if (downloader.getByteDownloaded() == downloader.getWholeBytes()) {
            if (!state.equals("COMPLETE")) {
                JOptionPane.showMessageDialog(new JFrame(), " Download File : " + downloader.getFileName() + " has successfully \n FINISHED");
                try {
                    Desktop.getDesktop().open(directory);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            state = "COMPLETE";
            FileWorker.changeBytesDownloadedInfile(this);
            FileWorker.removeFromQueue(this.getDownLoadQueueInformation());
            if (inQueue)
                if (HandleSchedular.queueData.size() > 0)
                    MiddlePanel.startDownloadInQueue(HandleSchedular.queueData.get(0));
        }
    }

    private String bytesPlusSuffix (long bytes){

        if (bytes / 1000000000 > 0)
            if (SettingFrame.language.equals("English"))
                return ( ((float)bytes) / 1000000000 ) + " GB" ;
            else
                return ( ((float)bytes) / 1000000000 ) + " گیگابایت " ;
        if (bytes / 1000000 > 0)
            if (SettingFrame.language.equals("English"))
                return ( ((float)bytes) / 1000000 ) + " MB" ;
            else
                return ( ((float)bytes) / 1000000 ) + "مگابایت " ;
        if (bytes / 1000 > 0)
            if (SettingFrame.language.equals("English"))
                return ( ((float)bytes) / 1000 ) + " KB" ;
            else
                return ( ((float)bytes) / 1000 ) + "کیلوبایت " ;
            if (SettingFrame.language.equals("English"))
                return (int)(bytes) + " Byte" ;
            else
                return (int)(bytes) + "بایت " ;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setState(String state) {
        this.state = state;
    }
}
