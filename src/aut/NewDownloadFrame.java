package aut;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.time.Period;
import java.util.ArrayList;

/**
 * this class represents a new frame to give the data of a new download
 * @author AmirHossein Rasulian
 */
public class NewDownloadFrame
{
    private JFrame newDownloadFrame ;
    private JButton ok;
    private JTextField urlAddress;
    private JButton saveAddress;
    private JTextField saveAdressTextFiled;
    private JButton cancel;
    private final String regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]\\p{all}*";
    public NewDownloadFrame ()
    {
        HandleActions handleActions = new HandleActions();

        newDownloadFrame = new JFrame("New Download");
            if (SettingFrame.language.equals("Persian"))
                newDownloadFrame.setTitle("دانلود جدید");
        newDownloadFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        newDownloadFrame.setLocationRelativeTo(null);
        newDownloadFrame.setMinimumSize(new Dimension(350 , 200));
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        newDownloadFrame.setContentPane(mainPanel);

        JPanel topOfPanel = new JPanel();
        SpringLayout topLayout = new SpringLayout();
        topOfPanel.setLayout(topLayout);
        JLabel urlName = new JLabel("URL Address");
        if (SettingFrame.language.equals("Persian"))
            urlName.setText("آدرس سرور");
        urlAddress = new JTextField();
        urlAddress.setPreferredSize(new Dimension(230 , 20));
        JLabel saveAddressName = new JLabel("Save Location");
        if (SettingFrame.language.equals("Persian"))
            saveAddressName.setText("محل ذخیره");
        saveAddress = new JButton();
        ImageIcon saveAddressIcon = new ImageIcon(".\\Icons\\fileChooser.png");
        saveAddress.setIcon(saveAddressIcon);
        saveAddress.setPreferredSize(new Dimension(20 , 20));
        saveAddress.setBackground(Color.GRAY);
        saveAddress.addActionListener(handleActions);
        saveAdressTextFiled = new JTextField(SettingFrame.defaultSaveLocationTextField.getText());
        saveAdressTextFiled.setPreferredSize(new Dimension(210,20));

        topOfPanel.add(urlName);
        topOfPanel.add(urlAddress);
        topOfPanel.add(saveAddressName);
        topOfPanel.add(saveAddress);
        topOfPanel.add(saveAdressTextFiled);
        topLayout.putConstraint(SpringLayout.WEST , urlName , 10 , SpringLayout.WEST , topOfPanel);
        topLayout.putConstraint(SpringLayout.EAST , urlAddress , 0 , SpringLayout.EAST , topOfPanel);
        topLayout.putConstraint(SpringLayout.WEST , saveAddressName , 10 , SpringLayout.WEST , topOfPanel);
        topLayout.putConstraint(SpringLayout.NORTH , saveAddressName , 10 , SpringLayout.SOUTH , urlName);
        topLayout.putConstraint(SpringLayout.EAST , saveAddress , 0 , SpringLayout.EAST , topOfPanel);
        topLayout.putConstraint(SpringLayout.NORTH , saveAddress , 5 ,SpringLayout.SOUTH , urlAddress);
        topLayout.putConstraint(SpringLayout.EAST , saveAdressTextFiled , 0 , SpringLayout.WEST , saveAddress);
        topLayout.putConstraint(SpringLayout.NORTH , saveAdressTextFiled , 5 ,SpringLayout.SOUTH , urlAddress);


        JPanel bottomOfPanel = new JPanel();
        bottomOfPanel.setLayout(new FlowLayout());

        ok = new JButton("Start");
        if (SettingFrame.language.equals("Persian"))
            ok.setText("شروع");
        ok.addActionListener(handleActions);

        cancel = new JButton("Cancel");
        if (SettingFrame.language.equals("Persian"))
            cancel.setText("لغو");
        cancel.addActionListener(handleActions);

        bottomOfPanel.add(ok);
        bottomOfPanel.add(cancel);


        JPanel middleOfPanel = new JPanel();


        mainPanel.add(topOfPanel);
        mainPanel.add(middleOfPanel);
        mainPanel.add(bottomOfPanel);

    }

    public void showFrame ()
    {
        newDownloadFrame.setVisible(true);
    }

    private class HandleActions implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ( e.getSource().equals(ok))
            {
                if (checkDownloadingIsOkForFilter() && checkDownloadingIsOkForBeforeExist() && checkDownloadingIsOkForURLNotCorrect()) {
                    NewDownload newDownload = new NewDownload(urlAddress.getText(), saveAdressTextFiled.getText());
                    MiddlePanel.addNewDownload(newDownload);
                    newDownloadFrame.setVisible(false);
                }
                else {
                    newDownloadFrame.setVisible(false);
                }
            }
            if ( e.getSource().equals(cancel))
                newDownloadFrame.setVisible(false);
            if (e.getSource().equals(saveAddress))
            {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new java.io.File("C:\\Users\\AHR96\\Desktop"));
                fileChooser.setDialogTitle("Default saving Location");
                if (SettingFrame.language.equals("Persian"))
                    fileChooser.setDialogTitle("محل ذخیره");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);

                int returnValue = fileChooser.showSaveDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    saveAdressTextFiled.setText( fileChooser.getSelectedFile().getAbsolutePath() );
                }
            }
        }

    }

    public boolean checkDownloadingIsOkForFilter (){
        String[] filterListNames = FileWorker.getFilterListNames() ;
        if (filterListNames == null)
            return true;
        for (String filter:
             filterListNames) {
            if (filter.equals(""))
                continue;
            if (urlAddress.getText().contains(filter)) {
                JOptionPane.showMessageDialog(new JFrame() , "Download this file is not possible \n the URL is in the blocked ones");
                return false;
            }
        }
        return true;
    }
    public boolean checkDownloadingIsOkForBeforeExist (){
        boolean result = MiddlePanel.checkThatDownloadExist(urlAddress.getText());
        if (!result)
            JOptionPane.showMessageDialog(new JFrame() , "Download this file is not possible \n" +
                    "u have this file in your download list");
            return result;
    }
    public boolean checkDownloadingIsOkForURLNotCorrect (){
        if (!urlAddress.getText().matches(regex)) {
            JOptionPane.showMessageDialog(new JFrame() , "Download this file is not possible \n" +
                    "the url that has entered is not correct");
            return false;
        }
        else
            return true;
    }
}
