package aut;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * this class represents the frame of the program where all other panels gather together and
 * makes the program frame
 * @author AmirHossein Rasulian
 */
public class ProgramFrame
{
    public static JFrame mainFrame ;
    private static MiddlePanel middlePanel;
    private static Dimension width;
    private boolean programIsInTrayMode ;
    public static SettingFrame settingFrame;

    public ProgramFrame ()
    {
        SettingFrame.changeLookAndFeel();

        mainFrame = new JFrame("Java Download Manager");
        if (SettingFrame.language.equals("Persian"))
            mainFrame.setTitle("نرم افزار دانلود جاوا");

        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                onExit();
            }
        });
        programIsInTrayMode = false ;

        mainFrame.setSize(1050 , 595);
        mainFrame.setLocation(300 , 200);

        JPanel framePanel = new JPanel();
        framePanel.setLayout(new BorderLayout());
        mainFrame.setContentPane(framePanel);

        ToolbarPanel toolBar = new ToolbarPanel();
        width = toolBar.getToolbarPanel().getPreferredSize() ;

        MenubarPanel menuBar = new MenubarPanel();
        framePanel.add(menuBar.getMenuPanel() , BorderLayout.NORTH) ;

        middlePanel = new MiddlePanel();
        JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(toolBar.getToolbarPanel() , BorderLayout.NORTH);
        rightPanel.add(middlePanel.getMiddlePanel() , BorderLayout.CENTER);

        framePanel.add(rightPanel,BorderLayout.CENTER);

        LeftPanel leftPanel = new LeftPanel();
        framePanel.add(leftPanel,BorderLayout.WEST);
        leftPanel.setPreferredSize(new Dimension(265 , 0));

        settingFrame = Main.settingFrame;
    }

    public void showFrame ()
    {
        mainFrame.setVisible(true);
    }

    public void onExit () {
        mainFrame.setVisible(false);
        if (!programIsInTrayMode) {
            TrayIcon trayIcon = null;
            if (SystemTray.isSupported()) {
                // get the SystemTray instance
                SystemTray tray = SystemTray.getSystemTray();
                // load an image
                BufferedImage image = null;
                try {
                    image = ImageIO.read(new File("./unnamed.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // create a action listener to listen for default action executed on the tray icon
                ActionListener listener = new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        // execute default action of the application
                        // ...
                        mainFrame.setVisible(true);
                    }
                };
                // create a popup menu
                PopupMenu popup = new PopupMenu();
                // create menu item for the default action
                /// ... add other items
                // construct a TrayIcon
                trayIcon = new TrayIcon(image, "Java DownLoad Manager", popup);
                // set the TrayIcon properties
                trayIcon.addActionListener(listener);
                // ...
                // add the tray image
                try {
                    tray.add(trayIcon);
                } catch (AWTException e) {
                    System.err.println(e);
                }
                // ...
            }
            programIsInTrayMode = true ;
        }
    }
}
