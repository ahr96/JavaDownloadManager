package aut;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * this is a class that has the frame of the setting of program
 * that has 3 options and 3 different panels to handle different works
 * @author AmirHossein Rasulian
 */
public class SettingFrame implements ListSelectionListener {
    private JFrame settingFrame ;
    private JButton fileChooserButton ;
    private JComboBox<String> lookAndFeelList;
    private JComboBox<String> filterList ;
    private String[] filterListNames ;
    public static JSpinner sameTimeDownloadsSpinner ;
    public static JTextField defaultSaveLocationTextField ;
    private DefaultComboBoxModel filterListModel ;
    private int selectedIndex = 0 ;

    private JPanel centerPanel ;
    private JPanel queueCenterPanel ;
    private JPanel schedulingCenterPanel ;
    private JButton general ;
    private JButton queuing ;
    private JButton scheduling ;
    private JPanel mainPanel;

    //queue panel components
    private static DefaultListModel<String> queueListModel;
    private JList queueList ;
    private JButton queueAddButton;
    private JButton queueSaveButton ;
    private JButton queueDeleteButton;
    private JButton queueUpButton;
    private JButton queueDownButton;
    private static final String upString = "Move up";
    private static final String downString = "Move down";

    public static String language ;
    private JComboBox<String> languageComboBox ;
    private JLabel languageLabel;

    private long dateStartLong ;
    private long dateEndLong ;
    private long dateAfterLong ;

    private JSpinner endTimeSpinner;
    private JSpinner startTimeSpinner;

    public static SpinnerDateModel dateModel;
    public static SpinnerDateModel dateModel2;
    private SpinnerDateModel dateModel3;

    public static boolean scheduleOnline ;
    private JRadioButton scheduleOnlineButton;

    private final String regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]\\p{all}*";

    public SettingFrame ()
    {
        ArrayList<String> settingData = FileWorker.getSettigsData();

        language = settingData.get(2);

        dateStartLong = Long.parseLong(settingData.get(3));
        dateEndLong = Long.parseLong(settingData.get(4));
        dateAfterLong = Long.parseLong(settingData.get(5));
        scheduleOnline = false ;
        if (settingData.get(6).equals("true"))
            scheduleOnline = true ;

        HandleActions handleActions = new HandleActions();

        settingFrame = new JFrame();

        settingFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        settingFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                String s ;
                if (scheduleOnline)
                    s = "true";
                else
                    s = "false";

                FileWorker.saveToQueue(queueListModel);
                FileWorker.changeSettings((Integer)sameTimeDownloadsSpinner.getValue() , defaultSaveLocationTextField.getText() , language , dateModel.getDate().getTime() , dateModel2.getDate().getTime() , dateModel3.getDate().getTime() , s);
                settingFrame.setVisible(false);
            }
        });
        settingFrame.setSize(600 , 500);
        settingFrame.setResizable(false);
        settingFrame.setLocation(400 , 250);

        mainPanel = new JPanel(new BorderLayout());
        settingFrame.setContentPane(mainPanel);

        JPanel leftPanel = new JPanel();
        BoxLayout leftPanelLayout = new BoxLayout(leftPanel,BoxLayout.Y_AXIS);
        leftPanel.setLayout(leftPanelLayout);
        general = new JButton("  General    ");
        if (SettingFrame.language.equals("Persian"))
            general.setText("عمومی");
        general.addActionListener(handleActions);
        queuing = new JButton("  Queuing   ");
        if (SettingFrame.language.equals("Persian"))
            queuing.setText("صف بندی");
        queuing.addActionListener(handleActions);
        scheduling = new JButton("Scheduling");
        if (SettingFrame.language.equals("Persian"))
            scheduling.setText("زمانبندی");
        scheduling.addActionListener(handleActions);

        leftPanel.add(general);
        leftPanel.add(queuing);
        leftPanel.add(scheduling);

        //Create the general center Panel and its elements

        centerPanel = new JPanel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine( 0 , 70 , 600 , 70);
                g.drawLine( 0 , 130 , 600 , 130 );
                g.drawLine( 0 , 200 , 600 , 200 );
                g.drawLine( 0 , 270 , 600 , 270 );
            }
        };
        SpringLayout layout = new SpringLayout();
        centerPanel.setLayout(layout);
        centerPanel.setBackground(Color.GRAY);
        // look and feel in setting
        JLabel lookAndFeelLabel = new JLabel("Look And Feel");
        if (SettingFrame.language.equals("Persian"))
            lookAndFeelLabel.setText("نوع نمایش");
        String[] lookAndFeelsNames = new String[] {
                "Metal Look And Feel" , "Windows Look And Feel" , "Motif Look And Feel" , "Windows Classic Look And Feel"
        };
        lookAndFeelList = new JComboBox<>(lookAndFeelsNames);
        lookAndFeelList.addActionListener(handleActions);
        // same time downloads in setting
        JLabel sameTimeDownloadsLabel = new JLabel("DownLoad");
        if (SettingFrame.language.equals("Persian"))
            sameTimeDownloadsLabel.setText("فایل همزمان");
        SpinnerModel sameTimeDownLoadsSpinnerNumber = new SpinnerNumberModel( Integer.parseInt(settingData.get(0)) , 0 , 100 , 1 );
        sameTimeDownloadsSpinner = new JSpinner(sameTimeDownLoadsSpinnerNumber);
        JLabel sameTimeDownloadsLabel2 = new JLabel("files at the same Time");
        if (SettingFrame.language.equals("Persian"))
            sameTimeDownloadsLabel2.setText("دانلود");
        //default save location in setting
        JLabel defaultSaveLocationLabel = new JLabel("Default Save Location");
        if (SettingFrame.language.equals("Persian"))
            defaultSaveLocationLabel.setText("محل ذخیره پیش فرض");
        defaultSaveLocationTextField = new JTextField(settingData.get(1));
        defaultSaveLocationTextField.setPreferredSize(new Dimension(300 , 20));
        fileChooserButton = new JButton();
        ImageIcon saveAddressIcon = new ImageIcon(".\\Icons\\fileChooser.png");
        fileChooserButton.setIcon(saveAddressIcon);
        fileChooserButton.setPreferredSize(new Dimension(20 , 20));
        fileChooserButton.setBackground(Color.GRAY);
        fileChooserButton.addActionListener(handleActions);

        JLabel filterLabel = new JLabel("List of Filter Sites : ");
        if (SettingFrame.language.equals("Persian"))
            filterLabel.setText(": لیست سایت های فیلتر شده");
        filterListNames = FileWorker.getFilterListNames() ;

        if (filterListNames != null)
            filterListModel = new DefaultComboBoxModel(filterListNames);
        else
            filterListModel = new DefaultComboBoxModel(new String[]{""});
        filterList = new JComboBox<>(filterListModel);

        filterList.setPreferredSize(new Dimension(300 ,20));
        filterList.setEditable(true);
        filterList.addActionListener(handleActions);

        languageLabel = new JLabel("Language : " + language);
        languageComboBox = new JComboBox<>(new String[]{ "English" , "Persian"});
        languageComboBox.addActionListener(handleActions);

        centerPanel.add(lookAndFeelLabel);
        centerPanel.add(lookAndFeelList);
        centerPanel.add(sameTimeDownloadsLabel);
        centerPanel.add(sameTimeDownloadsSpinner);
        centerPanel.add(sameTimeDownloadsLabel2);
        centerPanel.add(defaultSaveLocationLabel);
        centerPanel.add(defaultSaveLocationTextField);
        centerPanel.add(fileChooserButton);
        centerPanel.add(filterLabel);
        centerPanel.add(filterList);
        centerPanel.add(languageComboBox);
        centerPanel.add(languageLabel);

        layout.putConstraint(SpringLayout.NORTH , lookAndFeelLabel , 23 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , lookAndFeelLabel , 50 , SpringLayout.WEST , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , lookAndFeelList , 20 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , lookAndFeelList , 20 , SpringLayout.EAST , lookAndFeelLabel);
        layout.putConstraint(SpringLayout.NORTH , sameTimeDownloadsLabel , 90 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , sameTimeDownloadsLabel , 50 , SpringLayout.WEST , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , sameTimeDownloadsSpinner , 90 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , sameTimeDownloadsSpinner , 5 , SpringLayout.EAST , sameTimeDownloadsLabel);
        layout.putConstraint(SpringLayout.WEST , sameTimeDownloadsLabel2 , 5 , SpringLayout.EAST , sameTimeDownloadsSpinner);
        layout.putConstraint(SpringLayout.NORTH , sameTimeDownloadsLabel2 , 90 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , defaultSaveLocationLabel , 170 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , defaultSaveLocationLabel , 20 , SpringLayout.WEST , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , defaultSaveLocationTextField , 170 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , defaultSaveLocationTextField , 10 , SpringLayout.EAST , defaultSaveLocationLabel);
        layout.putConstraint(SpringLayout.NORTH , fileChooserButton , 170 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , fileChooserButton , 10 , SpringLayout.EAST , defaultSaveLocationTextField);
        layout.putConstraint(SpringLayout.NORTH , filterLabel , 230 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , filterLabel , 20 , SpringLayout.WEST , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , filterList , 230 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , filterList , 10 , SpringLayout.EAST , filterLabel);
        layout.putConstraint(SpringLayout.NORTH , languageComboBox , 300 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , languageComboBox , 250 , SpringLayout.WEST , centerPanel);
        layout.putConstraint(SpringLayout.NORTH , languageLabel , 303 , SpringLayout.NORTH , centerPanel);
        layout.putConstraint(SpringLayout.WEST , languageLabel , 120 , SpringLayout.WEST , centerPanel);

        mainPanel.add(leftPanel , BorderLayout.WEST);
        mainPanel.add(centerPanel , BorderLayout.CENTER);

        // create the  Queue Center Panel and Its Elements
        queueCenterPanel = new JPanel(new BorderLayout());
        queueCenterPanel.setBackground(Color.GRAY);

        JLabel queueLabel = new JLabel("File Name                                                      Size                                             ");
        if (SettingFrame.language.equals("Persian"))
            queueLabel.setText("                                                    حجم                                                         نام فایل");
        queueLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK , 1));

        queueListModel = new DefaultListModel<String>();
        for (String str:
             FileWorker.getQueues()) {
            queueListModel.addElement(str);
        }

        queueList = new JList(queueListModel);
        queueList.setPreferredSize(new Dimension(400 , 360));
        queueList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        queueList.addListSelectionListener(this);
        JScrollPane queueListScrollPane = new JScrollPane(queueList);

        //Create the list-modifying buttons.
        queueAddButton = new JButton("ADD");
        if (SettingFrame.language.equals("Persian"))
            queueAddButton.setText("اضافه");
        queueAddButton.addActionListener(new AddButtonListener());
        queueSaveButton = new JButton("SAVE");
        if (SettingFrame.language.equals("Persian"))
            queueSaveButton.setText("ذخیره");
        queueSaveButton.addActionListener(new AddButtonListener());

        queueDeleteButton = new JButton("DELETE");
        if (SettingFrame.language.equals("Persian"))
            queueDeleteButton.setText("پاک کردن");
        queueDeleteButton.addActionListener(new DeleteButtonListener());

        queueUpButton = new JButton("UP");
        if (SettingFrame.language.equals("Persian"))
            queueUpButton.setText("بالا");
        queueUpButton.addActionListener(new UpDownListener());
        queueUpButton.setActionCommand(upString);

        queueDownButton = new JButton("DOWN");
        if (SettingFrame.language.equals("Persian"))
            queueDownButton.setText("پایین");
        queueDownButton.addActionListener(new UpDownListener());
        queueDownButton.setActionCommand(downString);

        JPanel queueUpDownPanel = new JPanel(new GridLayout(2, 1));
        queueUpDownPanel.add(queueUpButton);
        queueUpDownPanel.add(queueDownButton);

        //Create a control panel, using the default FlowLayout.
        JPanel buttonPane = new JPanel();
        buttonPane.add(queueSaveButton);
        buttonPane.add(queueAddButton);
        buttonPane.add(queueDeleteButton);
        buttonPane.add(queueUpButton);
        buttonPane.add(queueDownButton);

        queueCenterPanel.add(queueListScrollPane , BorderLayout.CENTER);
        queueCenterPanel.add(queueLabel , BorderLayout.NORTH);
        queueCenterPanel.add(buttonPane , BorderLayout.PAGE_START);

        //create the scheduling panel and its components
        schedulingCenterPanel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(0 , 300 , 600 , 300);
            }
        };
        schedulingCenterPanel.setBackground(Color.GRAY);
        SpringLayout schLayout = new SpringLayout();
        schedulingCenterPanel.setLayout(schLayout);

        JLabel startTimeLabel = new JLabel("Start at : ");
        if (SettingFrame.language.equals("Persian"))
            startTimeLabel.setText(" : شروع از");
        Date startTimeDate = new Date();
        startTimeDate.setTime(dateStartLong);
        dateModel = new SpinnerDateModel(startTimeDate, null, null, Calendar.HOUR_OF_DAY);
        startTimeSpinner = new JSpinner(dateModel);
        JSpinner.DateEditor startEditor = new JSpinner.DateEditor(startTimeSpinner, "HH:mm");
        startTimeSpinner.setEditor(startEditor);

        JLabel endTimeLabel = new JLabel("End at :   ");
        if (SettingFrame.language.equals("Persian"))
            endTimeLabel.setText(" : پایان در");
        Date endTimeDate = new Date();
        endTimeDate.setTime(dateEndLong);
        dateModel2 = new SpinnerDateModel(endTimeDate, null, null, Calendar.HOUR_OF_DAY);
        endTimeSpinner = new JSpinner(dateModel2);
        JSpinner.DateEditor endEditor = new JSpinner.DateEditor(endTimeSpinner, "HH:mm");
        endTimeSpinner.setEditor(endEditor);

        JLabel startDlAfter = new JLabel("Start Download After : ") ;
        if (SettingFrame.language.equals("Persian"))
            startDlAfter.setText("دقیقه");
        JLabel minutes = new JLabel("minutes");
        if (SettingFrame.language.equals("Persian"))
            minutes.setText("شروع دانلود بعد از ");
        Date dateAfter = new Date();
        dateAfter.setTime(dateAfterLong);
        dateModel3 = new SpinnerDateModel(dateAfter, null, null, Calendar.HOUR_OF_DAY);
        JSpinner startAfterSpinner = new JSpinner(dateModel3);
        JSpinner.DateEditor startAfterEditor = new JSpinner.DateEditor(startAfterSpinner, "HH:mm");
        startAfterSpinner.setEditor(startAfterEditor);

        scheduleOnlineButton = new JRadioButton("Schedule : ");
        if (language.equals("Persian"))
            scheduleOnlineButton.setText("برنامه زمانی");

        if (scheduleOnline)
            scheduleOnlineButton.setSelected(true);
        scheduleOnlineButton.addActionListener(handleActions);

        schedulingCenterPanel.add(startTimeLabel);
        schedulingCenterPanel.add(startTimeSpinner);
        schedulingCenterPanel.add(endTimeLabel);
        schedulingCenterPanel.add(endTimeSpinner);
        schedulingCenterPanel.add(startDlAfter);
        schedulingCenterPanel.add(startAfterSpinner);
        schedulingCenterPanel.add(minutes);
        schedulingCenterPanel.add(scheduleOnlineButton);

        schLayout.putConstraint(SpringLayout.NORTH , scheduleOnlineButton , 20 , SpringLayout.NORTH , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , scheduleOnlineButton , 100 , SpringLayout.WEST , schedulingCenterPanel);

        schLayout.putConstraint(SpringLayout.NORTH , startTimeLabel , 150 , SpringLayout.NORTH , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , startTimeLabel , 170 , SpringLayout.WEST , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , startTimeSpinner , 20 , SpringLayout.EAST , startTimeLabel);
        schLayout.putConstraint(SpringLayout.NORTH , startTimeSpinner , 150 , SpringLayout.NORTH , schedulingCenterPanel);

        schLayout.putConstraint(SpringLayout.NORTH , endTimeLabel , 52 , SpringLayout.SOUTH , startTimeLabel);
        schLayout.putConstraint(SpringLayout.WEST , endTimeLabel , 170 , SpringLayout.WEST , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , endTimeSpinner , 20 , SpringLayout.EAST , endTimeLabel);
        schLayout.putConstraint(SpringLayout.NORTH , endTimeSpinner , 50 , SpringLayout.SOUTH , startTimeSpinner);

        schLayout.putConstraint(SpringLayout.NORTH , startDlAfter , 320 , SpringLayout.NORTH , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , startDlAfter , 110 , SpringLayout.WEST , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.NORTH , startAfterSpinner , 320 , SpringLayout.NORTH , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , startAfterSpinner , 10 , SpringLayout.EAST , startDlAfter);
        schLayout.putConstraint(SpringLayout.NORTH , minutes , 320 , SpringLayout.NORTH , schedulingCenterPanel);
        schLayout.putConstraint(SpringLayout.WEST , minutes , 10 , SpringLayout.EAST , startAfterSpinner);

    }

    public void showFrame (){
        settingFrame.setVisible(true);
    }

    class HandleActions implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(fileChooserButton))
            {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new java.io.File("C:\\Users\\AHR96\\Desktop"));
                fileChooser.setDialogTitle("Default saving Location");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);

                int returnValue = fileChooser.showSaveDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    defaultSaveLocationTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
            if (e.getSource().equals(lookAndFeelList)){
                String selectedItem = (String) lookAndFeelList.getSelectedItem() ;
                FileWorker.changeLookAndFeel(selectedItem);

                changeLookAndFeel();
            }
            if (e.getSource().equals(filterList)){
                int index = filterList.getSelectedIndex();
                if(index >= 0) {
                    selectedIndex = index;
                }
                else if("comboBoxEdited".equals(e.getActionCommand())) {
                    Object newValue = filterListModel.getSelectedItem();
                    if (filterListModel.getElementAt(selectedIndex).equals("")) {
                        if (((String)newValue).matches(regex)) {
                            FileWorker.addToFilterList((String) newValue);
                            filterListModel.addElement(newValue);
                            filterList.setSelectedItem(newValue);
                        }
                        else {
                            JOptionPane.showMessageDialog(new JFrame() , " the Address that u have entered is not correct for URL");
                            selectedIndex = 0 ;
                        }

                    }
//                    else {
//                        FileWorker.removeFromFilterList((String) filterListModel.getElementAt(selectedIndex));
//                        filterListModel.removeElementAt(selectedIndex);
//
//                        FileWorker.addToFilterList((String) newValue);
//                        filterListModel.addElement(newValue);
//                    }

//                    filterList.setSelectedItem(newValue);
//                    selectedIndex = filterListModel.getIndexOf(newValue);
//                    selectedIndex = 0 ;
                }
                if (!filterListModel.getElementAt(selectedIndex).equals(""))
                {
                    FileWorker.removeFromFilterList((String) filterListModel.getElementAt(selectedIndex));
                    filterListModel.removeElementAt(selectedIndex);
                    selectedIndex = 0 ;
                }

            }
            if (e.getSource().equals(queuing)) {
                mainPanel.remove(centerPanel);
                mainPanel.remove(schedulingCenterPanel);
                mainPanel.add(queueCenterPanel, BorderLayout.CENTER);
                settingFrame.revalidate();
                settingFrame.repaint();
            }
            if (e.getSource().equals(general)) {
                mainPanel.remove(queueCenterPanel);
                mainPanel.remove(schedulingCenterPanel);
                mainPanel.add(centerPanel, BorderLayout.CENTER);
                settingFrame.revalidate();
                settingFrame.repaint();
                System.out.println();
            }
            if (e.getSource().equals(scheduling)){
                mainPanel.remove(queueCenterPanel);
                mainPanel.remove(centerPanel);
                mainPanel.add(schedulingCenterPanel , BorderLayout.CENTER);
                settingFrame.revalidate();
                settingFrame.repaint();
                System.out.println("");
            }
            if (e.getSource().equals(languageComboBox)){
                language =(String) languageComboBox.getSelectedItem() ;
                languageLabel.setText("Language : " + language);
                if (language.equals("English"))
                    JOptionPane.showMessageDialog(new JFrame() , " for change the language reStart the program");
                else
                    JOptionPane.showMessageDialog(new JFrame() , "برای تغییر زبان برنامه را دوباره راه اندازی کنید");
            }
            if (e.getSource().equals(scheduleOnlineButton)) {
                if(scheduleOnlineButton.isSelected())
                    scheduleOnline = true;
                else
                    scheduleOnline = false ;

                if (language.equals("English"))
                    JOptionPane.showMessageDialog(new JFrame() , " for change the schedule reStart the program");
                else
                    JOptionPane.showMessageDialog(new JFrame() , "برای تغییر زمان برنامه را دوباره راه اندازی کنید");
            }
        }
    }

    public static void changeLookAndFeel(){
        try {
            UIManager.setLookAndFeel( FileWorker.getLookAndFeel() );
            for(Window window : JFrame.getWindows()) {
                SwingUtilities.updateComponentTreeUI(window);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    // queue listeners needed
//    class MyListDataListener implements ListDataListener {
//        public void contentsChanged(ListDataEvent e) {
//            queueLog.append("contentsChanged: " + e.getIndex0() + ", "
//                    + e.getIndex1() + newline);
//            queueLog.setCaretPosition(queueLog.getDocument().getLength());
//        }
//
//        public void intervalAdded(ListDataEvent e) {
//            queueLog.append("intervalAdded: " + e.getIndex0() + ", " + e.getIndex1()
//                    + newline);
//            queueLog.setCaretPosition(queueLog.getDocument().getLength());
//        }
//
//        public void intervalRemoved(ListDataEvent e) {
//            queueLog.append("intervalRemoved: " + e.getIndex0() + ", "
//                    + e.getIndex1() + newline);
//            queueLog.setCaretPosition(queueLog.getDocument().getLength());
//        }
//    }

    class DeleteButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            /*
             * This method can be called only if there's a valid selection, so
             * go ahead and remove whatever's selected.
             */

            ListSelectionModel lsm = queueList.getSelectionModel();
            int firstSelected = lsm.getMinSelectionIndex();
            int lastSelected = lsm.getMaxSelectionIndex();
            queueListModel.removeRange(firstSelected, lastSelected);

            int size = queueListModel.size();

            if (size == 0) {
                //List is empty: disable delete, up, and down buttons.
                queueDeleteButton.setEnabled(false);
                queueUpButton.setEnabled(false);
                queueDownButton.setEnabled(false);

            } else {
                //Adjust the selection.
                if (firstSelected == queueListModel.getSize()) {
                    //Removed item in last position.
                    firstSelected--;
                }
                queueList.setSelectedIndex(firstSelected);
            }
        }
    }

    /** A listener shared by the text field and add button. */
    class AddButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(queueAddButton)) {
                NewDownloadFrame newDownloadFrame = new NewDownloadFrame();
                newDownloadFrame.showFrame();
            }
            if (e.getSource().equals(queueSaveButton)){
                FileWorker.saveToQueue(queueListModel);
            }
        }
    }

    //Listen for clicks on the up and down arrow buttons.
    class UpDownListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //This method can be called only when
            //there's a valid selection,
            //so go ahead and move the list item.
            int moveMe = queueList.getSelectedIndex();

            if (e.getActionCommand().equals(upString)) {
                //UP ARROW BUTTON
                if (moveMe != 0) {
                    //not already at top
                    swap(moveMe, moveMe - 1);
                    queueList.setSelectedIndex(moveMe - 1);
                    queueList.ensureIndexIsVisible(moveMe - 1);
                }
            } else {
                //DOWN ARROW BUTTON
                if (moveMe != queueListModel.getSize() - 1) {
                    //not already at bottom
                    swap(moveMe, moveMe + 1);
                    queueList.setSelectedIndex(moveMe + 1);
                    queueList.ensureIndexIsVisible(moveMe + 1);
                }
            }
        }
    }

    //Swap two elements in the list.
    private void swap(int a, int b) {
        String aObject = queueListModel.getElementAt(a);
        String bObject = queueListModel.getElementAt(b);
        queueListModel.set(a, bObject);
        queueListModel.set(b, aObject);
    }

    //Listener method for list selection changes.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {

            if (queueList.getSelectedIndex() == -1) {
                //No selection: disable delete, up, and down buttons.
                queueDeleteButton.setEnabled(false);
                queueUpButton.setEnabled(false);
                queueDownButton.setEnabled(false);

            } else if (queueList.getSelectedIndices().length > 1) {
                //Multiple selection: disable up and down buttons.
                queueDeleteButton.setEnabled(true);
                queueUpButton.setEnabled(false);
                queueDownButton.setEnabled(false);

            } else {
                //Single selection: permit all operations.
                queueDeleteButton.setEnabled(true);
                queueUpButton.setEnabled(true);
                queueDownButton.setEnabled(true); }
        }
    }

    public static void addToQueue(String newString){
        queueListModel.addElement(newString);
        FileWorker.addToQueue(newString);
    }
}
