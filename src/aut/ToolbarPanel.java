package aut;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * this class represents the toolbar panel of the program that has buttons for certain jobs
 * @author AmirHossein Rasulian
 */
public class ToolbarPanel
{
    private Panel toolbarPanel;
    private JButton newDownload;
    private JButton pause;
    private JButton resume;
    private JButton cancel;
    private JButton remove;
    private JButton setting;

    private JButton sort ;
    private JButton deSort ;
    private JRadioButton sortByFile;
    private JRadioButton sortBySize;
    private JRadioButton sortByTime;

    private ArrayList<String> sortFactors ;

    private JTextField searchTextField ;

    public ToolbarPanel ()
    {
        HandleActions handleActions = new HandleActions();

        sortFactors = new ArrayList<>();

        toolbarPanel = new Panel();
        BoxLayout layout = new BoxLayout(toolbarPanel , BoxLayout.X_AXIS);
        toolbarPanel.setLayout(layout);
        toolbarPanel.setBackground(Color.WHITE);

        toolbarPanel.setPreferredSize(new Dimension(0 , 50));

        ImageIcon newIcon = new ImageIcon(".\\Icons\\add.png");
        newDownload = new JButton(newIcon);
        newDownload.setBackground(Color.WHITE);
        newDownload.addActionListener(handleActions);

        ImageIcon pauseIcon = new ImageIcon(".\\Icons\\pause.png");
        pause = new JButton(pauseIcon);
        pause.setBackground(Color.WHITE);
        pause.addActionListener(handleActions);

        ImageIcon resumeIcon = new ImageIcon(".\\Icons\\play.png");
        resume = new JButton(resumeIcon);
        resume.setBackground(Color.WHITE);
        resume.addActionListener(handleActions);

        ImageIcon cancelIcon = new ImageIcon(".\\Icons\\remove.png");
        cancel = new JButton(cancelIcon);
        cancel.setBackground(Color.WHITE);
        cancel.addActionListener(handleActions);

        ImageIcon removeIcon = new ImageIcon(".\\Icons\\taskcleaner.png");
        remove = new JButton(removeIcon);
        remove.setBackground(Color.WHITE);
        remove.addActionListener(handleActions);

        ImageIcon settingIcon = new ImageIcon(".\\Icons\\settings.png");
        setting = new JButton(settingIcon);
        setting.setBackground(Color.WHITE);
        setting.addActionListener(handleActions);

        JPanel options = new JPanel(new GridLayout(2,1));
        searchTextField = new JTextField("search");
            if (SettingFrame.language.equals("Persian"))
                searchTextField.setText("جستجو");
        searchTextField.addActionListener(handleActions);

        JPanel bottomOption = new JPanel(new GridLayout(1,4));
        sort = new JButton(new ImageIcon(".\\Icons\\up.png"));
        sort.setBackground(Color.WHITE);
        sort.addActionListener(handleActions);
        deSort = new JButton(new ImageIcon(".\\Icons\\down.png"));
        deSort.addActionListener(handleActions);
        deSort.setBackground(Color.WHITE);

        sortByFile = new JRadioButton("Name");
        if (SettingFrame.language.equals("Persian"))
            sortByFile.setText("نام");
        sortByFile.addActionListener(handleActions);
        sortBySize = new JRadioButton("Size");
        if (SettingFrame.language.equals("Persian"))
            sortBySize.setText("حجم");
        sortBySize.addActionListener(handleActions);
        sortByTime = new JRadioButton("Time");
        if (SettingFrame.language.equals("Persian"))
            sortByTime.setText("زمان");
        sortByTime.addActionListener(handleActions);

        JPanel sortPanelButon = new JPanel(new GridLayout(1,2));
        sortPanelButon.setPreferredSize(new Dimension(45 , 50));
        sortPanelButon.add(sort);
        sortPanelButon.add(deSort);
        bottomOption.add(sortPanelButon);
        bottomOption.add(sortByFile);
        bottomOption.add(sortBySize);
        bottomOption.add(sortByTime);

        options.add(searchTextField);
        options.add(bottomOption);

        toolbarPanel.add(newDownload);
        toolbarPanel.add(resume);
        toolbarPanel.add(pause);
        toolbarPanel.add(cancel);
        toolbarPanel.add(remove);
        toolbarPanel.add(setting);
        toolbarPanel.add(options);
    }

    public Panel getToolbarPanel() {
        return toolbarPanel;
    }

    private class HandleActions implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(newDownload))
            {
                NewDownloadFrame newDownload = new NewDownloadFrame();
                newDownload.showFrame();
            }
            if (e.getSource().equals(setting))
                ProgramFrame.settingFrame.showFrame();
            if (e.getSource().equals(remove))
                MiddlePanel.removeDownloads();
            if (e.getSource().equals(pause))
                MiddlePanel.pauseDownload();
            if (e.getSource().equals(resume))
                MiddlePanel.resumeDownload();
            if (e.getSource().equals(cancel))
                MiddlePanel.cancelDownload();
            if (e.getSource().equals(searchTextField)) {
                if (searchTextField.getText().equals(""))
                    MiddlePanel.backToNormalAfterSearch();
                else
                    MiddlePanel.search(searchTextField.getText());
            }
            if (e.getSource().equals(sortByFile))
            {
                if (sortByFile.isSelected())
                    sortFactors.add("fileName");
                else
                    sortFactors.remove("fileName");
            }
            if (e.getSource().equals(sortBySize)){
                if (sortBySize.isSelected())
                    sortFactors.add("size");
                else
                    sortFactors.remove("size");
            }
            if (e.getSource().equals(sortByTime))
            {
                if (sortByTime.isSelected())
                    sortFactors.add("time");
                else
                    sortFactors.remove("time");
            }
            if (e.getSource().equals(sort))
                MiddlePanel.sortOneFactor(sortFactors , "sort");
            if (e.getSource().equals(deSort))
                MiddlePanel.sortOneFactor(sortFactors , "deSort");
        }
    }
}
